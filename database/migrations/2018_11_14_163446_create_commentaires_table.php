<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commentaire', function(blueprint $table)
        {
            $table->increments('id');
            $table->Text('content');
            $table->string('user_id'); /*cela permet de savoir quel utilisateur à posté*/
            $table->integer('post-id'); /*pour des lignes de commentaire*/
            $table->timestamps();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
