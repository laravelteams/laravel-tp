<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('post', function(blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->Text('content');
            $table->integer('counts_comment');
            $table->integer('category_id');
            $table->user_id();
            $table->timestamps();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
