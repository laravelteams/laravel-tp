<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
     protected $guarded = ['id','create_at']; /*ce champs de modifier malgré que c'est moins securisé*/
}
